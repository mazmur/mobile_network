from flask import Flask, render_template, json, request, flash
# from content_management import Content
from flaskext.mysql import MySQL
from werkzeug import generate_password_hash, check_password_hash


app = Flask(__name__)

# MySQL configurations
app.config['MYSQL_DATABASE_HOST'] = 'localhost'
app.config['MYSQL_DATABASE_USER'] = 'root'
app.config['MYSQL_DATABASE_PASSWORD'] = ''
app.config['MYSQL_DATABASE_DB'] = 'latihan'
mysql = MySQL(app)

@app.route('/')
def main():
    return render_template('index.html')

@app.route('/showSignUp')
def showSignUp():
    return render_template('signup.html')

@app.route('/signUp',methods=['POST'])
def signUp():
    # if request.method == "POST":
    #     # details = request.form
    #     _name = request.form['inputName']
    #     _email = request.form['inputEmail']
    #     _password = request.form['inputPassword']
    #     cur = mysql.connect()
    #     cursor = cur.cursor()
    #     _hashed_password = generate_password_hash(_password)
    #     cursor.callproc('sp_createUser',(_name,_email,_hashed_password))
    #     # cursor.execute("INSERT INTO tbl_user(user_name, user_username, user_password) VALUES (%s, %s, %s)", (_name, _email, _hashed_password))
    #     cur.commit()
    #     cursor.close()
    #     return 'success'
    # return render_template('index.html')
    try:
        _name = request.form['inputName']
        _email = request.form['inputEmail']
        _password = request.form['inputPassword']

        # validate the received values
        if _name and _email and _password:
            
            # All Good, let's call MySQL
            
            conn = mysql.connect()
            cursor = conn.cursor()
            _hashed_password = generate_password_hash(_password)
            cursor.callproc('sp_createUser',(_name,_email,_hashed_password))
            # flash("User created successfully !")
            # cursor.execute("INSERT INTO tbl_user(user_name, user_username, user_password) VALUES (%s, %s, %s)", (_name, _email, _hashed_password))
            
            data = cursor.fetchall()

            if len(data) is 0:
                conn.commit()
                message = request.args.get("User created successfully !")
                return render_template("index.html",msg=message)
                # return json.dumps({'message':'User created successfully !'})
            else:
                return json.dumps({'error':str(data[0])})
        else:
            return json.dumps({'html':'<span>Enter the required fields</span>'})

    except Exception as e:
        return json.dumps({'error':str(e)})
    finally:
        cursor.close() 
        conn.close()

if __name__ == "__main__":
    app.run()