# Oncoding - Exercise :

Tools & software yang digunakan :
1.  [Instalasi Visual Studio Code](https://code.visualstudio.com/docs/setup/windows)
2.  [Instalasi Python](http://www.rebellionrider.com/install-python-3-windows-10-2/)
3.  [Instalasi MySql](https://dev.mysql.com/doc/workbench/en/wb-installing-windows.html)


Tahapan Instalasi Flask Menggunakan DBMS MySql:

* [ ]  Install virtualenv : 
        `pip install virtualenv`
* [ ]  Install virtualenvwrapper-win : `pip install virtualenvwrapper-win`
 
* [ ]  Membuat Virtual Environemt : ` mkvirtualenv Training `
 
* [ ]  Buat direktori project disimpan dan masuk kedalam direktori yang baru dibuat : `mkdir web`  + `cd web`

* [ ]  Set Project pada direktori kerja : `setprojectdir .`  

* [ ]  Jika ingin menon aktfikan Project Direktori : ` Deactive`

* [ ]  Cara masuk kedalam sebuah virtual enviroment : ` workon Training`

* [ ]  Instalasi Framework Flask pada virtual enviroment : `pip install flask`

* [ ]  Install module Mysql : `pip install flask-mysql`
 
* [ ]  Install mysql client : `pip install mysqlclient`


   


